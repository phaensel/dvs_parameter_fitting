# DVS_Parameter_Fitting
This git repository is part of a semester thesis. 
The purpose of this program is to approximate unkown parameters in a function by fitting it to optained mesurments. Hereby the algortihm is modulare and can be easily adjusted.

## TODOs
- Not all functionallaty from the commandline is yet impletmented into the gui

- Mac OS GUI does not work proper, problem lies within old tk verison on mac


## How to use
First install dependencies:
- Install Python3 for your system (3.9)
- (optional) Make a new python environment
```
$ python3 -m venv .env
$ source .env/bin/activate
```
- Install needed packages
```
pip install pandas matplotlib numpy scipy tqdm
```

- Install tkinter
    - Windows and Mac
    ```
    pip install tk
    ```
    - Linux
    ```
    sudo apt install python3-tk
    ```





### GUI
To use the algorithm there are two ways once by using the GUI.

```
python3 main.py --model "MODEL_NAME" --algorithm "ALGORITHM_NAME"
```

### Command Line

The full potential of the algorithm can be used using command line without gui, using flage "-g"

#### Create Data from Raw Data
We must bring the Raw Data into a format the algorthim can read. For this the function "create_data_form_raw_pickle" in "dataLoader.py" can be ajusted to read the raw inputfiles.


```
python3 main.py -g -c --data "PATH_TO_FILE_TO_STORE_DATA" --raw "PATH_TO_RAW_DATAFILE" --raw ... 
```
The file to store the processed data in must be .pickle file.

#### Run Algorithm

Run the algorithm using following command:

```
python3 main.py -g -r "./reports/REPORTNAME" --model "MODEL_NAME" --algorithm "ALGORITHM_NAME --data "PATH_TO_DATAFILE"
```

Select the model and the fitting algorithm and previously processed data file to use. 

By using flag -r a report of the final output is created under the given folder.



# Documentation

## DataLoader

To handle your raw datafiles extend or overrite the function "create_data_form_raw_pickle".

The final pickel file must be created with:
"pickle.dump((freq,numpy.asarray(inputData),numpy.asarray(outputData)),f)"

Here freq is the frequency range of the measurments for example [0,1,2,3,4,5,6...] (!Must be same for all measurments) (can also be time-domain or 2D or 3D space array depending on the model to use)

The inputData shell be a list of each input vector of all measurments. A input vector must the shape of the "input_shape" of the model.

The outputData must be a list of each output vector of all measurments must be same lenght as the freq.

## Models
The Model is used to handle the function to be approximated and its parameter. These parameter can also be dependend on the input.

### DVS Model
As can be read in the thesis the DVS model handels the noise model of a DVS pixel. It has parameter which are handled by seperat classes. To create modifications to these parameter, create a new class that implements the following functions:
```
class Parameter Model:
    def __init__(self) -> None:
        self.zero = 0
    
    def get(self):
        return self.zero
    
    def fitt(self, x, y):
        curve_fit(self.call_curveFit,x,numpy.log10(y),p0=self.get(),maxfev = 10000)
    
    def call_curveFit(self, x, zero):
        values = []
        self.zero = zero
        for cur in x:
            values.append(numpy.log10(self(*cur)))
        return values
    
    def __str__(self) -> str:
        pass
      
    def __call__(self, ipd, ipr) -> float:
        pass     
```

Then create a new Model that extends the DVSModel like:
```
class NewModel(DVSModel):
 def __init__(self, frequencyRange=numpy.logspace(-1, 5)) -> None:
        super().__init__(frequencyRange)
        self.cpd = NewParameter()
        self.cout = NewParameter()
        self.cfb = NewParameter()
        self.capacities = [self.cpd ,self.cout, self.cfb]
```

### Custom Model
A custom Model can be created by extending the class NoiseModel.
```
class NoiseModel:
    def __init__(self,parameter,input_shape) -> None:
        self.parameter = parameter
        self.inputShape = input_shape
    
    def __call__(self, x, parameter = self.parameter) -> Any:
        pass
    
    def graficalSettingsFrame(self,root) -> tk.Frame:
        pass
    
    def call_curveFit(self,x,*p):
        pass
```

The __init__ must be called be "super(parameter, input_shape)". Input_shape should look something like ["Name_of_input1", "Name_of_input2", ...].
The parameter are a list of parameter objects, can be just floats or ints but can also be objects that again implement a function depending on the inputs x.

The __call__ function must give pack the output of your function to be fitted for the given input x and the parameter.

The graficalSettingsFrame function can be used to create a TKinter page to tweek constants of the model using the GUI.

The call_curveFite function is used by the "PerSampleFitting" algortihm and must handle the to be fitted funtion as the scipy's curve_fit needs it.


## Algortihm 

Implement the new algorithm in the run function.

The report is created in the analyse function. To analyse your custom parameter implement the analyseParameter function.

The object history is used to store the output of each epoch of the fitting algorithm and is used to create the report.



```
class FittingAlgorithm:
    def __init__(self,model,inputData, outputData) -> None:
        self.model = model
        self.set_inputData(inputData)
        self.set_outputData(outputData)
        self.history = []
        self.history_columns = model.parameter 
        self.history_index = ""

    def save_parameter(self,parameter: np.ndarray):
        self.history.append(parameter)

    def save_parameter(self,parameter: list):
        self.history = self.history + parameter

    def set_inputData(self,data):
        self.input = data
    
    def set_outputData(self,data):
        self.output = data
        
    def run(self):
        pass
    
    def analyse(self, path = "./"):
        calcValues = []
        
        lossfunc = self.MeanSquardError
        
        for x in self.input:
            calcValues.append(self.model(x))
        
        err  = lossfunc(calcValues,self.output)
        
        self.plotError(err,path+"squard_error.png")
        maxError = np.max(err)
        minError = np.min(err)
        avgError = np.average(err)
        
        f = open(path+"report.txt", "w")
        f.write(f"Analysis Summary: \n Using loss {lossfunc.__name__}\n \n max Error: {maxError}\n min Error: {minError}\n avg Error: {avgError}")
        f.close()
        
        self.analyseParameter(path)
        
        
    def analyseParameter(self,path):
        pass
        
    def plotError(self,error, path):
        fig = pp.figure()
        axes = fig.subplots(len(self.model.inputShape))
        for ax,inp,name in zip(axes,np.transpose(self.input),self.model.inputShape):
            ax.plot(inp,error,"*")
            ax.set_title(f"error vs {name}")
        fig.legend()
        # fig.title("Sqaurd error vs Inputs")
        fig.savefig(path)
        
    def MaxAbsError(self, x_calc, x_real):
        err = np.max(np.abs(x_calc-x_real),axis=1)
        return err
            
    def MeanSquardError(self, x_calc, x_real):
        err = self.model.weight**2*(x_calc-x_real)**2
        return np.sum(err,axis=1)
    
        
    
    def graficalSettingsFrame(self,root) -> tk.Frame:
        pass  
```



