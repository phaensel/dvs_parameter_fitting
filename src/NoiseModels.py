import numpy
from src.dvs_pixel_model import dvs_pixel
from typing import Any

from scipy.optimize import curve_fit

import tkinter as tk
import json



class NoiseModel:
    def __init__(self,parameter,input_shape) -> None:
        self.parameter = parameter
        self.inputShape = input_shape
    
    def __call__(self, x, parameter) -> Any:
        pass
    
    def graficalSettingsFrame(self,root) -> tk.Frame:
        pass
    
    def call_curveFit(self,x,*p):
        pass
    
    
class DVSModel(NoiseModel):
    Va_fb=500
    Va_amp_n=500
    Va_amp_p=500
    kappa_n=0.75
    kappa_p=0.8
    temp = 25
    Rpd=1e15
    
    def __init__(self,frequencyRange = numpy.logspace(-1,5)) -> None:
        super().__init__(["Cpd","Cout","Cfb"],["Ipd","Ipr"])
        self.dvs = dvs_pixel(0,0,0,0,0,0,0,self.Va_fb,self.Va_amp_n,self.Va_amp_p,self.kappa_n,self.kappa_p,25,self.Rpd)
        self.freq = frequencyRange

        self.ipdIndex = 0
        self.iprIndex = 1
        self.cpd = ConstantCapacity()
        self.cout = ConstantCapacity()
        self.cfb = ConstantCapacity()
        self.capacities = [self.cpd ,self.cout, self.cfb]
        self.bounds = [[10**-15,10**-14,10**-15],[10**-12,10**-12,10**-12]]
              
    def __call__(self, x: numpy.ndarray, parameterVector=None) -> Any:
        
        if (x.ndim == 1): x = numpy.expand_dims(x,0)
        if (x.ndim != 2): raise(AttributeError(f"Input vector x has wrong dim, should be 2 but is {x.ndim}"))
        if (len(self.inputShape) != x.shape[1]):
            raise(AttributeError("Input vector x does not match inputShape"))
        if type(parameterVector) != None:
            self.set_parameter(parameterVector)
            
        out = []
        
        for data in x:
            self.set_underlying_model(data[0],data[1])
            out.append(self.calculate_PSD())
            
        return numpy.asarray(out) if len(out) > 1 else out[0]
    
    def call_curveFit(self,x,*p):
        self.set_initialParameters(*p)
        self.set_underlying_model(x[0],x[1])
        return self.calculate_PSD()*self.weight
         
    def set_frequencyRange(self,freq):
        self.freq = freq
        self.weight = numpy.append(numpy.diff(numpy.log10(freq)),0)
            
    def calculate_PSD(self):
        # print(f"Ipd: {self.dvs.Ipd}")
        # print(f"Ipr: {self.dvs.Ipr}")
        # print(f'Cpd {self.dvs.Cpd}, Cout {self.dvs.Cout}, Cfb {self.dvs.Cfb}')
        return numpy.log10(self.dvs.pr_noise_psd(2j*numpy.pi*self.freq)) 
        
    def set_initialParameters(self,cpd,cout,cfb):
        self.dvs.Cout = cout
        self.dvs.Cpd = cpd
        self.dvs.Cfb = cfb
        self.cpd.zero = cpd
        self.cout.zero = cout
        self.cfb.zero = cfb
        if not hasattr(self,"res"):
            self.res = [cpd,cout,cfb]
    
    def reset(self):
        self.set_initialParameters(*self.res)
       
    def set_underlying_model(self,ipd,ipr):
        self.dvs.Ipd = ipd
        self.dvs.Ipr = ipr
        self.dvs.Cpd = self.cpd(ipd,ipr)
        self.dvs.Cout = self.cout(ipd,ipr)
        self.dvs.Cfb = self.cfb(ipd,ipr)
    
    def set_parameter(self,parameterVector):
        self.set_initialParameters(*parameterVector)

    def get_parameter(self):
        return [self.dvs.Cpd,self.dvs.Cout,self.dvs.Cfb]
                     
    def graficalSettingsFrame(self,root):
        self.frame = tk.Frame(root)
        #Model Type
        tk.Label(self.frame,text=f"Modeltype: {type(self).__name__}").grid(column=0,row=0)
        
        #Input
        tk.Label(self.frame,text="Input").grid(column=0,row=1)
        inputstr = tk.StringVar(value=str(self.inputShape))
        
        tk.Entry(self.frame,textvariable=inputstr).grid(column=1,row=1)
        
        #Parameter
        tk.Label(self.frame,text="Parameter").grid(column=0,row=2)
        parstr = tk.StringVar(value=str(self.parameter))
        tk.Entry(self.frame,textvariable=parstr).grid(column=1,row=2)
        
        #Vafb
        tk.Label(self.frame,text="Va_fb").grid(column=0,row=3)
        Va_fb = tk.DoubleVar(value=self.Va_fb)
        tk.Entry(self.frame,textvariable=Va_fb).grid(column=1,row=3)
        
        #Va_amp_n
        tk.Label(self.frame,text="Va_amp_n").grid(column=0,row=4)
        Va_amp_n = tk.DoubleVar(value=self.Va_amp_n)
        tk.Entry(self.frame,textvariable=Va_amp_n).grid(column=1,row=4)
        
        #Va_amp_p
        tk.Label(self.frame,text="Va_amp_p").grid(column=0,row=5)
        Va_amp_p = tk.DoubleVar(value=self.Va_amp_p)
        tk.Entry(self.frame,textvariable=Va_amp_p).grid(column=1,row=5)
        
        #kappa_n
        tk.Label(self.frame,text="kappa_n").grid(column=0,row=6)
        kappa_n = tk.DoubleVar(value=self.kappa_n)
        tk.Entry(self.frame,textvariable=kappa_n).grid(column=1,row=6)
        
        #kappa_p
        tk.Label(self.frame,text="kappa_p").grid(column=0,row=7)
        kappa_p = tk.DoubleVar(value=self.kappa_p)
        tk.Entry(self.frame,textvariable=kappa_p).grid(column=1,row=7)
        
        #temp
        tk.Label(self.frame,text="temp").grid(column=0,row=8)
        temp = tk.DoubleVar(value=self.temp)
        tk.Entry(self.frame,textvariable=temp).grid(column=1,row=8)
        
        #Rpd
        tk.Label(self.frame,text="Rpd").grid(column=0,row=9)
        Rpd = tk.DoubleVar(value=self.Rpd)
        tk.Entry(self.frame,textvariable=Rpd).grid(column=1,row=9)
        
        #Savebutton
        def save():
            print(inputstr.get())   
            self.inputShape = json.loads(inputstr.get())
            self.parameterName = json.loads(parstr.get())
            self.Va_fb = Va_fb.get()
            self.Va_amp_n = Va_amp_n.get()
            self.Va_amp_p = Va_amp_p.get()
            self.kappa_n = kappa_n.get()
            self.kappa_p = kappa_p.get()
            self.temp = temp.get()
            self.Rpd = Rpd.get()
            
            
    
        tk.Button(self.frame,text="Save",command=save).grid(column=0,row=10)
        
        return self.frame
    



    #c = co + 1/sqrt(A * log(ipr))
        
        
class DVS_linar_dependent_capacities(DVSModel):
    def __init__(self, frequencyRange=numpy.logspace(-1, 5)) -> None:
        super().__init__(frequencyRange)
        self.cpd = LinearCapacity()
        self.cout = LinearCapacity()
        self.cfb = LinearCapacity()
        self.capacities = [self.cpd ,self.cout, self.cfb]
    
    
class DVS_physical_capacities(DVSModel):
    def __init__(self, frequencyRange=numpy.logspace(-1, 5)) -> None:
        super().__init__(frequencyRange)
        self.cpd = PhotoDiodCapacity()
        self.cout = OutputCapacity()
        self.cfb = FeedBackCapacity()
        self.capacities = [self.cpd ,self.cout, self.cfb]
     
 
class FeedBackCapacity:
    def __init__(self) -> None:
        self.zero = 1e-15
        self.a = 5e-15
        self.b = 1
        self.r = 0.5
        self.d = 0.0005
        self.v = 1
        self.const = True
    
    def get(self):
        return [self.zero, self.a, self.b, self.r,self.d,self.v]
    
    def set(self, zero, a, b, c, d):
        self.zero = zero
        self.a = a
        self.b = b
        self.c = c
        self.d = d
    
    def fitt(self,x,y):
        curve_fit(self.call_curveFit,x,numpy.log10(y),self.get(),maxfev=10000)
    
    def call_curveFit(self, x, zero, a , b, r, d,v):
        values = []
        self.zero = zero
        self.a = a
        self.r = r
        self.d = d
        self.v = v
        for cur in x:
            values.append(numpy.log10(self(*cur)))
        return values
    
    def __str__(self) -> str:
        #pass
        return f"{self.zero} + {self.a}/(1+exp({self.r}*ipr - {self.d} * log10(ipd)+{self.v})) + {self.b}/(1+exp(-{self.r}*ipr + {self.d} * log10(ipd)+{self.v}))"
      
    def __call__(self,ipd, ipr) -> float:
        if self.const: return self.zero
        x = self.r*ipr - self.d * numpy.log10(ipd)
        return self.zero + self.a/(1+numpy.exp(x+self.v))
        #return self.zero + self.a * (x+self.v)**2
    

class PhotoDiodCapacity:
    def __init__(self) -> None:
        self.bounds = [[0,0,-20],[1,1,0]]
        self.zero = 0
        self.a = 1e-14
        self.b = -11.3
        self.const = True
    
    def get(self):
        return [self.zero, self.a, self.b]
    
    def set(self, zero, a, b):
        self.zero = zero
        self.a = a
        self.b = b
    
    def fitt(self, x, y):
        curve_fit(self.call_curveFit,x,numpy.log10(y),p0=self.get(),maxfev = 10000) 
      
    def call_curveFit(self, x, zero, a ,b):
        values = []
        self.zero = zero
        self.a = a
        self.b = b
        for cur in x:
            values.append(numpy.log10(self(*cur)))
        return values
    
    def __str__(self) -> str:
        return f"{self.zero} + {self.a}/sqrt(1 + log(ipd)/{self.b})"
      
    def __call__(self, ipd, ipr) -> float:
        if self.const: return self.zero
        return self.zero + self.a/numpy.sqrt(numpy.abs((1 + numpy.log10(ipd)/self.b)))    

class OutputCapacity:
    def __init__(self) -> None:
        self.bounds = [[1e-18,-20,-20,],[1e-10,0,0]]
        self.zero = 0
        self.a = -12
        self.b = -13.5
        self.c = -13.7
        self.d = -7.8
        self.const = True
    
    def get(self):
        return [self.zero, self.a, self.b,self.c,self.d]
    
    def set(self, zero, a, b, c, d):
        self.zero = zero
        self.a = a
        self.b = b
        self.c = c
        self.d = d
    
    def fitt(self, x, y):
        curve_fit(self.fitt_a,x,numpy.log10(y),p0=[self.zero,self.a,self.b], maxfev = 10000)
        curve_fit(self.fitt_c,x,numpy.log10(y),p0=[self.zero,self.c,self.d],maxfev = 10000)
    
    def fitt_a(self, x, zero, a, b):
        values = []
        self.zero = zero
        self.a = a
        self.b = b
        for cur in x:
            values.append(numpy.log10(self(*cur)))
        return values
    
    def fitt_c(self, x, zero, c, d):
        values = []
        self.zero = zero
        self.c = c
        self.d = d
        for cur in x:
            values.append(numpy.log10(self(*cur)))
        return values
    
    def __str__(self) -> str:
        return f"{self.zero} + {10**self.a}/sqrt(1 + ipr/{10**self.b}) + {10**self.c}/sqrt(1 - ipr/{10**self.d}) "
      
    def __call__(self, ipd, ipr) -> float:
        if self.const: return self.zero
        return numpy.nan_to_num(self.zero + (10**self.a)/numpy.sqrt((1 + ipr/(10**self.b))) + (10**self.c)/numpy.sqrt((1 - ipr/10**(self.d))))   
        
        
            
class ConstantCapacity:
    def __init__(self) -> None:
        self.zero = 0
    
    def get(self):
        return self.zero
    
    def fitt(self, x, y):
        curve_fit(self.call_curveFit,x,numpy.log10(y),p0=self.get(),maxfev = 10000)
    
    def call_curveFit(self, x, zero):
        values = []
        self.zero = zero
        for cur in x:
            values.append(numpy.log10(self(*cur)))
        return values
    
    def __str__(self) -> str:
        return f"{self.zero}"
      
    def __call__(self, ipd, ipr) -> float:
        return self.zero       
             
class LinearCapacity:
    def __init__(self) -> None:
        self.slopeIpd = 0
        self.slopeIpr = 0
        self.zero = 0
    
    def get(self):
        return [self.slopeIpd, self.slopeIpr, self.zero]
      
    def get_slope(self):
        return [self.slopeIpd, self.slopeIpr]
        
    def set_slope(self, slopeIpd, slopeIpr):
        self.slopeIpd = slopeIpd
        self.slopeIpr = slopeIpr
    
    def fitt(self, x, y):
        curve_fit(self.call_curveFit,x,numpy.log10(y),p0=self.get(),maxfev = 10000)
    
    def call_curveFit(self, x, sipd, sipr, zero):
        values = []
        for cur in x:
            self.set_slope(sipd,sipr)
            self.zero = zero
            values.append(numpy.log10(self(*cur)))
        return values
    
    def __str__(self) -> str:
        return f"{self.slopeIpd} * ipd + {self.slopeIpr} * ipr + {self.zero}"
      
    def __call__(self, ipd, ipr) -> float:
        return self.slopeIpd * ipd + self.slopeIpr * ipr + self.zero