from random import randint
import numpy as np
import scipy


class dvs_pixel:
    q=1.602e-19
    kb=1.38e-23

    


    def __init__(self,Ipd,Ipr,Isf,Cpd,Cout,Cfb,Csf,Va_fb,Va_amp_n,Va_amp_p,kappa_n,kappa_p,temp,Rpd,inDC=10**-3.73,inF=10**-0.77,ground= 10**-11.67,groundfreq= 10**4.24,pr_topology='sf' ):
        self.Ipd=Ipd
        self.Ipr=Ipr
        self.Isf=Isf
        self.Cpd=Cpd
        self.Csf=Csf
        self.Cout=Cout
        self.Cfb=Cfb
        self.Va_fb=Va_fb
        self.Va_amp_n=Va_amp_n
        self.Va_amp_p=Va_amp_p
        self.kappa_n=kappa_n
        self.kappa_p=kappa_p
        self.temp=temp+273.15
        self.Ut=self.kb*self.temp/self.q
        self.Rpd=Rpd
        self.pr_topology=pr_topology
        self.inDC = inDC
        self.inF = inF
        self.groundNiveau = ground
        self.groundFreq = groundfreq

    def gs_fb(self):
        return self.Ipd/self.Ut
    def gm_fb(self):
        return self.kappa_n*self.Ipd/(self.Ut)
    def gm_amp_n(self):
        return self.kappa_n*self.Ipr/(self.Ut)
    def ro_fb(self):
        return self.Va_fb/self.Ipd
    def ro_amp_n(self):
        return self.Va_amp_n/self.Ipr
    def ro_amp_p(self):
        return self.Va_amp_p/self.Ipr
    def Cin(self):
        return self.Cpd
    def Gm1(self):
        if(self.pr_topology=='sf'): 
            return self.gm_fb()
        if(self.pr_topology=='cg'): 
            return self.gs_fb()+1/self.ro_fb()
    def Gm2(self):
        if(self.pr_topology=='sf'): 
            return self.gm_amp_n()
        if(self.pr_topology=='cg'): 
            return self.gm_amp_n()-1/self.ro_fb()
    def Rin(self):
        if(self.pr_topology=='sf'): 
            return  1/(self.gs_fb()+1/self.ro_fb()+1/self.Rpd)
            #return  1/(self.gs_fb()+1/self.ro_fb())
        if(self.pr_topology=='cg'): 
            return  1/(1/self.ro_fb()+1/self.Rpd)
            #return  self.ro_fb()
    def Rout(self):
        if(self.pr_topology=='sf'): 
            return (self.ro_amp_n()*self.ro_amp_p())/(self.ro_amp_n()+self.ro_amp_p())
        if(self.pr_topology=='cg'): 
            return  1/(self.gs_fb()+1/self.ro_fb()+1/self.ro_amp_n()+1/self.ro_amp_p())
    def Aloop(self):
        return self.Gm1()*self.Rin()*self.Gm2()*self.Rout()
    def Zin_dc(self):
        return self.Rin()/(self.Aloop()+1)
    def Zm_dc(self):
        return 1/self.Gm1()*self.Aloop()/(self.Aloop()+1)
    def Zout_dc(self):
        return self.Rout()/(self.Aloop()+1)
    def coef_s_num_Zin(self):
        return(self.Cout+self.Cfb)*self.Rout()
    def coef_s_num_Zm(self):
        return self.Cfb/self.Gm2()
    def coef_s_num_Zout(self):
        return (self.Cin()+self.Cfb)*self.Rin()
    def coef_s2_den(self):
        return (self.Cfb*(self.Cin()+self.Cout)+self.Cin()*self.Cout)*self.Rin()*self.Rout()/(self.Aloop()+1)
    def coef_s_den(self):
        return ((self.Cin()+self.Cfb*(1+self.Gm2()*self.Rout()))*self.Rin()+(self.Cout+self.Cfb*(1-self.Gm1()*self.Rin()))*self.Rout())/(self.Aloop()+1)
    def Zin(self,s):
        den=s**2*self.coef_s2_den()+s*self.coef_s_den()+1
        num_Zin=self.Zin_dc()*(1+s*self.coef_s_num_Zin())
        return np.abs(num_Zin/den)
    def Zm(self,s):
        den=s**2*self.coef_s2_den()+s*self.coef_s_den()+1
        num_Zm=self.Zm_dc()*(1+s*self.coef_s_num_Zm())
        return np.abs(num_Zm/den)
    def Zm_relI(self,s):
        return self.Zm(s)*self.Ipd
    def Zm_sf_relI(self,s):
        return self.Zm_relI(s)*self.Asf(s)
    def Zout(self,s):
        den=s**2*self.coef_s2_den()+s*self.coef_s_den()+1
        num_Zout=self.Zout_dc()*(1+s*self.coef_s_num_Zout())
        return np.abs(num_Zout/den)
    def Asf(self,s):
        gs_sf=self.Isf/self.Ut
        wc=gs_sf/self.Csf
        den=s/wc+1
        num=self.kappa_p
        return np.abs(num/den)
    def Zout_sf(self,s):
        gs_sf=self.Isf/self.Ut
        wc=gs_sf/self.Csf
        den=s/wc+1
        num=1/gs_sf
        return np.abs(num/den)
    def pr_noise_psd_ipd(self,s):
        return 2*2*self.q*self.Ipd*self.Zm(s)**2
    def pr_noise_psd_ipr(self,s):
        return 2*2*self.q*self.Ipr*self.Zout(s)**2
    def pr_noise_psd(self,s):
        return self.pr_noise_psd_ipr(s)+self.pr_noise_psd_ipd(s)+self.integral_noise_psd(s)+np.abs(self.groundNiveau/(1 + s/self.groundFreq))
    def sf_noise_psd_pr(self,s):
        return self.pr_noise_psd(s)*self.Asf(s)**2
    def sf_noise_psd_sf(self,s):
        return 2*2*self.q*self.Isf*self.Zout_sf(s)**2
    def sf_noise_psd(self,s):
        return self.sf_noise_psd_pr(s)+self.sf_noise_psd_sf(s)
    def pr_noise_sqrtpsd_ipr(self,s):
        return np.sqrt(self.pr_noise_psd_ipr(s))
    def pr_noise_sqrtpsd_ipd(self,s):
        return np.sqrt(self.pr_noise_psd_ipd(s))
    def pr_noise_sqrtpsd(self,s):
        return np.sqrt(self.pr_noise_psd(s))
    def sf_noise_sqrtpsd_pr(self,s):
        return np.sqrt(self.sf_noise_psd_pr(s))
    def sf_noise_sqrtpsd_sf(self,s):
        return np.sqrt(self.sf_noise_psd_sf(s))
    def sf_noise_sqrtpsd(self,s):
        return np.sqrt(self.sf_noise_psd(s))
    def pr_noise_psd_integral_ipd(self,s):
        return scipy.integrate.cumtrapz(self.pr_noise_psd_ipd(s),np.abs(s),initial=0)
    def pr_noise_psd_integral_ipr(self,s):
        return scipy.integrate.cumtrapz(self.pr_noise_psd_ipr(s),np.abs(s),initial=0)
    def pr_noise_psd_integral(self,s):
        return scipy.integrate.cumtrapz(self.pr_noise_psd(s),np.abs(s),initial=0)
    def sf_noise_psd_integral_pr(self,s):
        return scipy.integrate.cumtrapz(self.sf_noise_psd_pr(s),np.abs(s),initial=0)
    def sf_noise_psd_integral_sf(self,s):
        return scipy.integrate.cumtrapz(self.sf_noise_psd_sf(s),np.abs(s),initial=0)
    def sf_noise_psd_integral(self,s):
        return scipy.integrate.cumtrapz(self.sf_noise_psd(s),np.abs(s),initial=0)
    def pr_noise_psd_sqrtintegral_ipd(self,s):
        return np.sqrt(self.pr_noise_psd_integral_ipd(s))
    def pr_noise_psd_sqrtintegral_ipr(self,s):
        return np.sqrt(self.pr_noise_psd_integral_ipr(s))
    def pr_noise_psd_sqrtintegral(self,s):
        return np.sqrt(self.pr_noise_psd_integral(s))
    def sf_noise_psd_sqrtintegral_sf(self,s):
        return np.sqrt(self.sf_noise_psd_integral_sf(s))
    def sf_noise_psd_sqrtintegral_pr(self,s):
        return np.sqrt(self.sf_noise_psd_integral_pr(s))
    def sf_noise_psd_sqrtintegral(self,s):
        return np.sqrt(self.sf_noise_psd_integral(s))
    def gen_white_noise(self,t,f):
        self.noise_f=f
        self.noise_t=t
        scale_factor=np.sqrt(2*2*np.pi*(f[1]-f[0]))
        noise_ph_ipd=np.random.uniform(0,2*np.pi,len(f))
        noise_ph_ipd_matrix=np.reshape(np.repeat(noise_ph_ipd,len(t)),(len(f),len(t)))
        #self.white_noise_matrix_ipd=np.sin(np.outer(2*np.pi*f,t)+noise_ph_ipd_matrix)/np.sqrt(len(f)/2)
        self.white_noise_matrix_ipd=np.sin(np.outer(2*np.pi*f,t)+noise_ph_ipd_matrix)*scale_factor
        noise_ph_ipr=np.random.uniform(0,2*np.pi,len(f))
        noise_ph_ipr_matrix=np.reshape(np.repeat(noise_ph_ipr,len(t)),(len(f),len(t)))
        #self.white_noise_matrix_ipr=np.sin(np.outer(2*np.pi*f,t)+noise_ph_ipr_matrix)/np.sqrt(len(f)/2)
        self.white_noise_matrix_ipr=np.sin(np.outer(2*np.pi*f,t)+noise_ph_ipr_matrix)*scale_factor
        noise_ph_isf=np.random.uniform(0,2*np.pi,len(f))
        noise_ph_isf_matrix=np.reshape(np.repeat(noise_ph_isf,len(t)),(len(f),len(t)))
        #self.white_noise_matrix_isf=np.sin(np.outer(2*np.pi*f,t)+noise_ph_isf_matrix)/np.sqrt(len(f)/2)
        #self.white_noise_matrix_isf=np.sin(np.outer(2*np.pi*f,t)+noise_ph_isf_matrix)
        self.white_noise_matrix_isf=np.sin(np.outer(2*np.pi*f,t)+noise_ph_isf_matrix)*scale_factor
    def pr_ipd_noise(self):
        #psd_vs_noise_f=np.sqrt(self.pr_noise_psd_ipd(2*np.pi*complex(0,1)*self.noise_f))
        psd_vs_noise_f=np.sqrt(self.pr_noise_psd_ipd(2*np.pi*complex(0,1)*self.noise_f))
        psd_vs_noise_f_matrix=np.reshape(np.repeat(psd_vs_noise_f,len(self.noise_t)),(len(self.noise_f),len(self.noise_t)))
        return np.sum(np.multiply(self.white_noise_matrix_ipd,psd_vs_noise_f_matrix),axis=0)
    def pr_ipr_noise(self):
        #psd_vs_noise_f=np.sqrt(self.pr_noise_psd_ipr(2*np.pi*complex(0,1)*self.noise_f))
        psd_vs_noise_f=np.sqrt(self.pr_noise_psd_ipr(2*np.pi*complex(0,1)*self.noise_f))
        psd_vs_noise_f_matrix=np.reshape(np.repeat(psd_vs_noise_f,len(self.noise_t)),(len(self.noise_f),len(self.noise_t)))
        return np.sum(np.multiply(self.white_noise_matrix_ipr,psd_vs_noise_f_matrix),axis=0)

    def integral_noise_psd(self,s):
        return np.abs(self.inDC/(1+s*self.inF))**2
