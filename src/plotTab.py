import tkinter as tk
from tkinter import ttk
import json
import numpy
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg,NavigationToolbar2Tk

from src.fitting import GradientDescent
from src.NoiseModels import DVSModel


class PlotTab(ttk.Frame):
    
    def __init__(self,tabControl,model,input,output) -> None:
        super().__init__(tabControl)
        self.model = model
        self.input = input
        self.output = output
        self.plotFrame()
        self.selecteFrame()

    
    def plotFrame(self):
        self.plotframe = ttk.Frame(self)
        # the figure that will contain the plot
        
        self.fig = Figure()
        self.ax = self.fig.add_subplot(111)
        self.canvas = FigureCanvasTkAgg(self.fig,
                                master = self.plotframe)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack()
        
        toolbar = NavigationToolbar2Tk(self.canvas,
                                    self.plotframe)
        toolbar.update()

        # placing the toolbar on the Tkinter window
        
        self.plotframe.pack(side="left")
    
    def plot(self, outputIndex, fittedIndex):
        #clearPlot
        if hasattr(self,"fig"):
            self.ax.clear()
        self.ax.set_ylabel("$S_{vp}(f)$ $[V^2/Hz]$")
        self.ax.set_xlabel("Frequency [Hz]")
        self.ax.grid(True)
        
        
        for i in outputIndex:
            self.ax.plot(numpy.log10(self.model.freq),self.output[i])
            
        for i in fittedIndex:
            self.ax.plot(numpy.log10(self.model.freq),self.model(self.input[i]))
        self.fig.canvas.draw()
    
    
    def selecteFrame(self):
        canvas = tk.Canvas(self)
        scrollbar = ttk.Scrollbar(self, orient="vertical", command=canvas.yview)
        scrollable_frame = ttk.Frame(canvas)

        scrollable_frame.bind(
            "<Configure>",
            lambda e: canvas.configure(
                scrollregion=canvas.bbox("all")
            )
        )
        canvas.create_window((0, 0), window=scrollable_frame, anchor="nw")
        canvas.configure(yscrollcommand=scrollbar.set)
        
        ori = []
        fit = []
        
        def updatePlot():
            if len(ori) != len(self.output):
                raise RuntimeError("Number of Checkboxes do not meet lenght of output")
            orin=[]
            for i, o in enumerate(ori):
                if o.get():
                    orin.append(i)
            fitin=[]
            for i, o in enumerate(fit):
                if o.get():
                    fitin.append(i)
                    
            self.plot(orin,fitin)
                
        for i,name in enumerate(self.model.inputShape): 
            tk.Label(scrollable_frame,text=f'{name}').grid(column=i,row=0)
        tk.Label(scrollable_frame,text=f'Mes').grid(column=len(self.model.inputShape),row=0)
        tk.Label(scrollable_frame,text=f'Fit').grid(column=len(self.model.inputShape)+1,row=0)

        
        for i,input in enumerate(self.input):
            tk.Label(scrollable_frame,text=f'{input[0]}').grid(column=0,row=i+1)
            tk.Label(scrollable_frame,text=f'{input[1]}').grid(column=1,row=i+1)
            ori.append(tk.BooleanVar())
            tk.Checkbutton(scrollable_frame,variable=ori[i],command=updatePlot).grid(column=2,row=i+1)
            fit.append(tk.BooleanVar())
            tk.Checkbutton(scrollable_frame,variable=fit[i],command=updatePlot).grid(column=3,row=i+1)
            
        scrollbar.pack(side="right", fill="y")
        # scrollable_frame.pack()
        canvas.pack(side="right", fill="both", expand=True)
        #container.pack(fill="both",padx=40, pady=40)
        
        

