from src.dvs_pixel_model import dvs_pixel
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

class CurrentApproximation:
    def __init__(self) -> None:
        pass
    
    def run(self,x: np.array):
        pass
    
class LinearCurrent(CurrentApproximation):
    def __init__(self,slope) -> None:
        super().__init__()
        self.a = slope
        
    def run(self,x: np.array):
        return self.a * x
    
    
class DVSipdApproximation(CurrentApproximation):
    def __init__(self,freq) -> None:
        super().__init__()
        self.inDC=10**-3.73
        self.inF=10**-0.77
        self.freq = freq
    
    def run(self,x:np.array, psd: np.array):
        if x.shape[0] != psd.shape[0]:
            return
        
        for i, p in zip(x,psd):
            kernel = np.ones((20,))/20
            plt.loglog(self.freq,p)
            p = p - self.integral_noise_psd(2j*np.pi*self.freq)
            p = np.convolve(kernel,p,'same')
            dcLevel = np.max(p)
            plt.loglog(self.freq,p)
            plt.loglog(self.freq,self.integral_noise_psd(2j*np.pi*self.freq))
            
        plt.show()
            
        
    def integral_noise_psd(self,s):
        return np.abs(self.inDC/(1+s*self.inF))**2
    
class DVSInputApprox(CurrentApproximation):
    Va_fb=500
    Va_amp_n=500
    Va_amp_p=500
    kappa_n=0.75
    kappa_p=0.8
    temp = 25
    Rpd=1e15
    
    def __init__(self, frequencyRange = np.logspace(-1,3), Cpd=3e-14, Cout=2.75e-14, Cfb=1e-15) -> None:
        super().__init__()
        self.dvs = dvs_pixel(0,0,0,Cpd,Cout,Cfb,0,self.Va_fb,self.Va_amp_n,self.Va_amp_p,self.kappa_n,self.kappa_p,25,self.Rpd)
        self.freq = frequencyRange
              
    def calculate_PSD(self, x , ipd):
        self.dvs.Ipd = ipd
        return self.dvs.pr_noise_psd(2j*np.pi*x)    
    
    def run(self, x: np.array, y: np.array, ipdIndex = 0, iprIndex = 1):
        ipd = []
        for c, out in zip(x,y):
            self.dvs.Ipr = c[iprIndex]
            p, r = curve_fit(self.calculate_PSD,self.freq,out,p0=c[ipdIndex])
            ipd.append(p)
            
        print(ipd)
        return np.array(ipd)
        