import tkinter as tk
from tkinter import ttk
import json
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg,NavigationToolbar2Tk

from src.fitting import GradientDescent
from src.NoiseModels import DVSModel

from src.plotTab import PlotTab

class Gui:
    
    loadFunction = None 
    fitFunction = None
    
    tabs = []
    
    def __init__(self,model,fittingAlgo) -> None:
        self.model = model
        self.fittingAlgo = fittingAlgo
    
    def show(self):
        # the main Tkinter window
        self.window = tk.Tk()

        # setting the title
        self.window.title('NoiseFitting')

        # dimensions of the main window
        self.window.geometry("1200x800")
        
        menubar = tk.Menu(self.window)
        self.window.config(menu=menubar)

        fileMenu = tk.Menu(menubar)
        fileMenu.add_command(label="Load", command=self.select_file)
        fileMenu.add_command(label="Export", command=lambda: self.exportFunction(self.model,self.fittingAlgo))
        fileMenu.add_command(label="Exit", command=lambda : exit())
        menubar.add_cascade(label="File", menu=fileMenu)
        
        dataMenu = tk.Menu(menubar)
        dataMenu.add_command(label="Run", command=self.fitFunction)
        menubar.add_cascade(label="Run", menu=dataMenu)
        

        fitMenu = tk.Menu(menubar)
        fitMenu.add_command(label="Edit")
        fitMenu.add_command(label="Save")
        menubar.add_cascade(label="Plot", menu=fitMenu)
        
        modelMenu = tk.Menu(menubar)
        modelMenu.add_command(label="Model", command=self.modelWindows)
        modelMenu.add_command(label="Fitting Algorithm", command=self.fittingWindow)
        modelMenu.add_command(label="Data", command=self.create_dataSettings)
        modelMenu.add_command(label="Plot")
        menubar.add_cascade(label="Windows", menu=modelMenu)
        
        
        
        self.tabControl = ttk.Notebook(self.window)
        self.tabControl.pack(expand = 1, fill ="both")
        
        
        self.plotTab = PlotTab(self.tabControl,self.model,self.fittingAlgo.input,self.fittingAlgo.output)
        self.tabControl.add(self.plotTab, text ='Plot')
        self.tabControl.select(self.plotTab)

        # run the gui
        self.window.mainloop()
    
    def select_file(self):
        filetypes = (
            ('Pickel files', '*.pkl'),
            ('CSV files', '*.csv'),
            ('All files', '*.*')
        )
        filename = tk.filedialog.askopenfilename(
            title='Open a file',
            initialdir='~',
            filetypes=filetypes)

        self.loadFunction(filename)
        
    def fittingWindow(self) -> ttk.Frame:
        if hasattr(self,"fittingTab"):
            return
        self.fittingTab = self.fittingAlgo.graficalSettingsFrame(self.tabControl)
        self.tabControl.add(self.fittingTab, text ='Fitting')
        self.tabControl.select(self.fittingTab)
                   
    def modelWindows(self):
        
        if hasattr(self,"modelTab"):
            return
        
        self.modelTab = self.model.graficalSettingsFrame(self.tabControl)
        
        self.tabControl.add(self.modelTab, text ='Model')
        self.tabControl.select(self.modelTab)
    
    def create_dataSettings(self):
        frame = ttk.Frame(self.tabControl)
        
        frame.columnconfigure(0,weight=1)
        frame.columnconfigure(1,weight=4)
        
        tk.Label(frame,text="Data").grid(column=0,row=0)
        
        self.tabs.append(frame)
        self.tabControl.add(frame, text ='Data Settings')
        self.tabControl.select(frame)    
    

        

   
if __name__ == "__main__":
    Gui()
