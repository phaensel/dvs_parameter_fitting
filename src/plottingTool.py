import pickle
import tkinter as tk
from tkinter import ttk
import pandas as pd
import matplotlib.pyplot as pp
from matplotlib.widgets import Slider, Button
from dvs_pixel_model import dvs_pixel
import numpy as np
import gradient as g
from scipy.optimize import curve_fit

with open('./data/noise_event_rate_array_study_with_testpixel_var_PrBp_20230107.pkl','rb') as f:
    data=pickle.load(f)
    
    data=data.sort_values('illuminance')

    #data['Ipd'] = 7.03e-14*data['illuminance'] - 6.68e-17
    data['Ipd'] = (10**-13.25)*data['illuminance']**1.115
    10*-12

#data = data[data['iPrBp_target']<3.6e-9].iloc[::,:]


savedata = pd.DataFrame(columns=["illuminance","iPrBp_target","Ipd","Cpd min","Cpd max","Cout min","Cout max","Cfb min","Cfb max",
                                 "Cpd error +-","Cout error +-","Cfb error +-"])
csvFile = "save.csv"
# print(data["iPrBp_target"])
# for i in range(len(data)):
#     dc = np.max(data.iloc[i]['pr_psd']['psd'])
#     savedata.loc[i] = {
#                         "illuminance": data.iloc[i]['illuminance'],
#                         "iPrBp_target": data.iloc[i]['iPrBp_target'],
#                         "dc-level": dc 
#                     }
    
# data = data.merge(savedata,'outer')

Cpd=3e-14
Cout=2.75e-14
Cfb=1e-15

Va_fb=500
Va_amp_n=500
Va_amp_p=500
kappa_n=0.75
kappa_p=0.8
Ut=0.025 
Rpd=1e15

ground = 10**-11.67
groundFreq = 10**4.24

class Curve:
    def __init__(self) -> None:
        pass  
    

def illumiToIPD(L):
    return 10**(-13) * L

class PlotWindow:
    
    def __init__(self,freq) -> None:
        self.ipds = []
        self.cpd = []
        self.cout = []
        self.cfb = []

        self.freq = freq
        self.psd = []
        self.dc = 10**-3.73
        self.parameter = ["kappa","Cpd","Cout","Cfb","Rpd","DC level","Integral Freq"]
        self.values = [10-9,Cpd,Cout,Cfb,Rpd,ground,groundFreq]
        
        self.sliders = []
    
    def setDVS(self, Ipd, Ipr) -> None:
        self.dvs = dvs_pixel(Ipd,Ipr,0,Cpd,Cout,Cfb,0,Va_fb,Va_amp_n,Va_amp_p,kappa_n,kappa_p,25,Rpd,self.dc,10**-0.77,ground,groundFreq)
        self.values[0] = Ipd
        
    def setPSD(self,psd):
        self.psd = psd
    
    def update(self,val):
        self.dvs.Ipd = 10**self.sliders[0].val
        self.dvs.Cpd = 10**self.sliders[1].val
        self.dvs.Cout = 10**self.sliders[2].val
        self.dvs.Cfb = 10**self.sliders[3].val
        self.dvs.Rpd = 10**self.sliders[4].val
        self.dvs.groundNiveau = 10**self.sliders[5].val
        self.dvs.groundFreq = 10**self.sliders[6].val
        
        self.dvslane.set_ydata(self.dvs.pr_noise_psd(2j*np.pi*self.freq))
        # self.losslane.set_ydata(self.lossfunction(self.freq,self.psd))
        # self.dclane.set_ydata(np.repeat(self.dc,len(self.freq)))
        self.fig.canvas.draw_idle()
    
    def onclick(self,event):

        if event.inaxes != self.axs[0]: return

        ix, iy = event.xdata, event.ydata
        
        self.sliders[5].set_val(np.log10(iy))

        return 

    def plot(self,id,illumi,dc,Ipd,ipr):
        self.il = illumi
        self.ipr = ipr
        self.id = id
        self.ipd = Ipd
        self.dc = dc
        # self.values[5] = dc
        self.values[0] = Ipd

        self.fig, self.axs = pp.subplots(nrows=len(self.parameter)+1, ncols=1, gridspec_kw={'width_ratios':[1], 'height_ratios':[2*len(self.parameter)]+[1,1,1,1,1,1,1]})
        self.psdlane, = self.axs[0].loglog(self.freq,self.psd)
        self.dvslane, = self.axs[0].loglog(self.freq,self.dvs.pr_noise_psd(2j*np.pi*self.freq))
        # self.losslane, = self.axs[0].plot(self.freq,self.lossfunction(self.freq,self.psd))
        # self.dclane, = self.axs[0].plot(self.freq,np.repeat(self.dc,len(self.freq)),linestyle ="--")
        self.fig.suptitle(f"Ipd Clac: {Ipd}, IPr: {ipr}")


        cid = self.fig.canvas.mpl_connect('button_press_event', self.onclick)


        axsave = self.fig.add_axes([0.70, 0, 0.1, 0.075])
        bsave = Button(axsave, 'Save')
        bsave.on_clicked(self.save)

        axstore = self.fig.add_axes([0.55, 0, 0.1, 0.075])
        bstore = Button(axstore, 'Store')
        bstore.on_clicked(self.store)

        axnext = self.fig.add_axes([0.85, 0, 0.1, 0.075])
        bnext = Button(axnext, 'fit')
        bnext.on_clicked(self.fit)
        ax = self.axs[1]
        # ax = pp.axes([0.25, 0 - i*0.05, 0.65, 0.05])
        self.sliders.append(Slider(ax, "Ipd", -20, -8, Ipd))
        self.sliders[0].on_changed(self.update)
        for i,par in enumerate(self.parameter):
            if i == 0: continue
            ax = self.axs[i+1]
            # ax = pp.axes([0.25, 0 - i*0.05, 0.65, 0.05])
            self.sliders.append(Slider(ax, par, np.log10(self.values[i])-5, np.log10(self.values[i])+5, np.log10(self.values[i])))
            self.sliders[i].on_changed(self.update)
        pp.show()
    
    def store(self,val):
        print("stored")
        self.ipds.append(self.dvs.Ipd)
        self.cpd.append(self.dvs.Cpd)
        self.cout.append(self.dvs.Cout)
        self.cfb.append(self.dvs.Cfb)

    def save(self,val):
        np.savetxt("freq.csv",self.psdlane.get_xdata(), delimiter=",")
        np.savetxt("meas.csv",self.psdlane.get_ydata(), delimiter=",")
        np.savetxt("calc.csv",self.dvslane.get_ydata(), delimiter=",")
        # savedata.loc[self.id] = {  
        #                 "illuminance": self.il,
        #                 "iPrBp_target": self.dvs.Ipr,
        #                 "Ipd": self.dvs.Ipd,
        #                 "Cpd min": min(self.cpd),
        #                 "Cpd max": max(self.cpd),
        #                 "Cout min": min(self.cout),
        #                 "Cout max": max(self.cout),
        #                 "Cfb min": min(self.cfb),
        #                 "Cfb max": max(self.cfb)
        #             }
        # print(f"saved: {savedata.loc[self.id]}")
        # savedata.to_csv(csvFile)

    def noiseFunction(self,x,cpd,cout,cfb):
        self.dvs.Cpd = 10**cpd
        self.dvs.Cout=10**cout
        self.dvs.Cfb=10**cfb
        # self.dvs.Ipd = 10**i
        return self.logfunction(x)
    
    def logfunction(self,x):
        return np.log10(self.dvs.pr_noise_psd(2j*np.pi*x))

    def lossfunction(self,x,y):
        return (np.log10(y) - self.logfunction(x))**2
    
    def loss(self,x,y,par):
        self.dvs.Cpd = 10**par[0]
        self.dvs.Cout = 10**par[1]
        self.dvs.Cfb = 10**par[2]
        self.dvs.Rpd = 10**par[3]
        return np.average((np.log10(y) - np.log10(self.dvs.pr_noise_psd(2j*np.pi*x)))**2)
    
    def fit(self,dummy):
        p0 =  np.log10([self.dvs.Cpd,self.dvs.Cout,self.dvs.Cfb])
        
        p,cov = curve_fit(self.noiseFunction,self.freq,np.log10(self.psd),p0,maxfev = 10000)
        
        print()

        # p = g.gradient_descent(g.gradient,self.loss,np.asarray([self.freq]),np.asarray(self.psd),p0) 
        print(p)
        self.sliders[1].set_val(p[0])
        self.sliders[2].set_val(p[1])
        self.sliders[3].set_val((p[2]))
        # self.sliders[0].set_val((p[3]))
        self.update(dummy)
         

class PlotButton:
    
    ploted = []
    
    
    def __init__(self,root,id):
        self.id = id
        self.b = tk.Button(root,text="plot",fg='black', bg='red',command= lambda: self.plot())
        
    
    def plot(self):
        if self.id in PlotButton.ploted:
            self.b.configure(bg="red")
            PlotButton.ploted.remove(self.id)
        else:
            self.b.configure(bg="green")
            PlotButton.ploted.append(self.id)
        
        # plotax = pp.loglog(,data.iloc[id]['pr_psd']['psd'])
            
        self.win = PlotWindow(data.iloc[self.id]['pr_psd']['freq'])
        self.win.setPSD(data.iloc[self.id]['pr_psd']['psd'])
        self.win.setDVS(data.iloc[self.id]['Ipd'],data.iloc[self.id]['iPrBp_target'])
        self.win.plot(self.id,data.iloc[self.id]['illuminance'],10,data.iloc[self.id]['Ipd'], data.iloc[self.id]['iPrBp_target'])
        
        
class Table:
     
    def __init__(self,root,data):
        # code for creating table
        # print(data)
        data = data[['illuminance','iPrBp_target','Ipd']]

        

        col = 1
        for column in data:
            self.e = tk.Label(root, text=column, width=10, fg='blue',
                               font=('Arial',16,'bold'))
                 
            self.e.grid(row=0, column=col)
            # self.e.insert(tk.END, column)
            for i in range(len(data)):

                t = float('%.4g' % data.iloc[i][column])
                if t == 0: t = data.iloc[i][column]
                self.e = tk.Label(root, text= t,width=10, fg='blue',
                               font=('Arial',16,'bold'))
                 
                self.e.grid(row=i+1, column=col)
                # self.e.insert(tk.END, )
                # row +=  data.iloc[i][col] + " : "
            
            
            col += 1
            # print(row)    
            
        for i in range(len(data)):
            b = PlotButton(root,i)
            b.b.grid(row=i+1, column=col)


    

root = tk.Tk()
container = ttk.Frame(root)
canvas = tk.Canvas(container)
scrollbar = ttk.Scrollbar(container, orient="vertical", command=canvas.yview)
h = ttk.Scrollbar(container, orient="horizontal", command=canvas.xview)
scrollable_frame = ttk.Frame(canvas)

scrollable_frame.bind(
    "<Configure>",
    lambda e: canvas.configure(
        scrollregion=canvas.bbox("all")
    )
)

canvas.create_window((0, 0), window=scrollable_frame, anchor="nw")

canvas.configure(yscrollcommand=scrollbar.set)
canvas.configure(xscrollcommand=h.set)

t = Table(scrollable_frame,data)
h.pack(side="bottom", fill="x")
scrollbar.pack(side="right", fill="y")
container.pack(fill="both",expand=True)
canvas.pack(side="left", fill="both", expand=True)



root.mainloop()
