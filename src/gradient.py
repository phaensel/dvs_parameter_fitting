import pickle
import numpy as np
import matplotlib.pyplot as plot

class GradientDescent:

    def __init__(self, model, x, y, start, learn_rate=0.0001,
                 n_iter=100, tolerance=1e-06,
                inLogScale = False, parameterInLogScale = False) -> None:
        
        self.x = x
        self.y = y
        self.model = model
        self.start = start
        self.learn_rate = learn_rate
        self.n_iter = n_iter
        self.tolerance = tolerance
        self.inLogScale = inLogScale
        self.parameterInLogScale = parameterInLogScale
        
            
        self.history = []
        
        
        self.gradient = self.gradient_function
        
        if not callable(self.gradient):
            raise TypeError("'gradient' must be callable")
        
        if x.shape[0] != y.shape[0]:
            raise ValueError("'x' and 'y' lengths do not match")
        
        self.dtype_= np.float32
        
        self.learn_rate = np.array(learn_rate, dtype=self.dtype_)

        self.n_iter = int(n_iter)

        
        
        if self.n_iter <= 0:
            raise ValueError("'n_iter' must be greater than zero")

        # Setting up and checking the tolerance
        self.tolerance = np.array(tolerance, dtype=self.dtype_)
        if np.any(tolerance <= 0):
            raise ValueError("'tolerance' must be greater than zero")

        if inLogScale:
            self.weight = np.append(np.diff(np.log10(model.freq)),0)
        else:
            self.weight = np.ones(y[0].shape)
        self.weight = self.weight/np.linalg.norm(self.weight)

        
    def run(self):  
        vector = np.array(self.start, dtype=self.dtype_)       
        self.history.append(vector)
        
        # Performing the gradient descent loop
        for i in range(self.n_iter):
            # Recalculating the difference
            error = self.squard_error_loss(vector)
            print(f'iter {i}/{self.n_iter}: error: {error}')
            print(f'vector: {vector}')
        
            # if np.all(np.abs(grad) <= self.tolerance):
            #     print(f"|Gradient| is smaller then tolerance {self.tolerance} so finished at epoch {i}")
            #     break
            grad = self.gradient(vector)
            print(grad)
            diff = self.learn_rate*grad
            if self.parameterInLogScale:
                vector /= 10**diff
            else:
                vector -= diff
            self.history.append(np.append(vector,error))  
            # Checking if the absolute difference is small enough
            # if np.all(np.abs(diff) <= tolerance):
            #     break

            # Updating the values of the variables
        return [vector, self.history] if vector.shape else [vector.item(), self.history]

    def squard_error_loss(self,par): 
        se = (self.y - self.model(self.x,par))**2
        error = np.matmul(se,self.weight)
        return np.average(error)
        
    #jeder parameter nur auf einen gewissen region berechenen (interfall)
        
    def gradient_function(self,par):
        grad = []
        deltaValue = 0.01
        deltaLog = 10**deltaValue
        for i in range(len(par)):
            delta = np.zeros(len(par))
            delta[i] = deltaValue
            if self.parameterInLogScale:
                grad.append((self.squard_error_loss(par*deltaLog)-self.squard_error_loss(par/deltaLog))/(2*deltaValue))
            else:   
                grad.append((self.squard_error_loss(par+delta)-self.squard_error_loss(par-delta))/(2*deltaValue))
        return grad
        
            

            