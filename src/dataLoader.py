import pandas
import pickle
import numpy


def load_data(file: str, type: str, inputShape: list = ["freq"] ,xIndex: str = "input", yIndex: str = "output"):
    data = pandas.DataFrame()
    if (type == "csv"):
        print("loading csv not implemented")
        return
    elif type == "pickle":
        data = load_pickle(file)
        filter = data[0] > 1
        filterl = numpy.repeat([filter],repeats=data[1].shape[0],axis=0)
        print(data[1].shape)
        return [data[0][filter],  data[1], data[2][:,filter] ]
    
    if data.empty:
        print(f"Error when reading file: {file}")


def load_pickle(file: str) -> pandas.DataFrame:
    with open(file,'rb') as f:
        data = pickle.load(f)
        if(len(data) != 3): raise "Pickel file must have shape (freq, input, output) like (numpy.array, numpy.array, numpy.array)"
        if(data[1].shape[0] != data[2].shape[0] or data[0].shape[0] != data[2].shape[1]): raise "Shape of in and output do not match"
        return data

def create_data_form_raw_pickle(files: list, outputFile: str):
    df = pandas.DataFrame(columns=["input","output"])
    inputData = []
    outputData = []
    freq = []
    for file in files:
        with open(file,'rb') as f:
            data=pickle.load(f)
            
            freq = numpy.array(data.iloc[0]['pr_psd']['freq'])
            for i in range(len(data)):
                #print(f"error: {numpy.sum(freq-numpy.array(data.iloc[0]['pr_psd']['freq']))}")
                inputData.append( [ (10**-13)*data.iloc[i]['illuminance'], data.iloc[i]['iPrBp_target']] ),
                outputData.append( numpy.array(numpy.log10(data.iloc[i]['pr_psd']['psd']) ))
                    
    with open(outputFile,'wb') as f:
        pickle.dump((freq,numpy.asarray(inputData),numpy.asarray(outputData)),f)
        
       

# def create_data_form_raw_pickle(file: str, outputFile: str):
#     df = pandas.DataFrame(columns=["input","output"])
#     with open(file,'rb') as f:
#         data=pickle.load(f)
#         inputData = []
#         outputData = []
#         freq = numpy.array(data.iloc[0]['pr_psd']['freq'])
#         for i in range(len(data)):
#             #print(f"error: {numpy.sum(freq-numpy.array(data.iloc[0]['pr_psd']['freq']))}")
#             inputData.append( [ (10**-13)*data.iloc[i]['illuminance'], data.iloc[i]['iPrBp_target']] ),
#             outputData.append( numpy.array(numpy.log10(data.iloc[i]['pr_psd']['psd']) ))
                   
#     with open(outputFile,'wb') as f:
#         pickle.dump((freq,numpy.asarray(inputData),numpy.asarray(outputData)),f)
        
    