import pickle
import tkinter as tk
import pandas as pd
import matplotlib.pyplot as pp
from src.dvs_pixel_model import dvs_pixel
import numpy as np
import src.gradient as g
from scipy.optimize import curve_fit
import tqdm 

class FittingAlgorithm:
    def __init__(self,model,inputData, outputData) -> None:
        self.model = model
        self.set_inputData(inputData)
        self.set_outputData(outputData)
        self.history = []
        self.history_columns = model.parameter 
        self.history_index = ""

    def save_parameter(self,parameter: np.ndarray):
        self.history.append(parameter)

    def save_parameter(self,parameter: list):
        self.history = self.history + parameter

    def set_inputData(self,data):
        self.input = data
    
    def set_outputData(self,data):
        self.output = data
        
    def run(self):
        pass
    
    def analyse(self, path = "./"):
        calcValues = []
        
        lossfunc = self.MeanSquardError
        
        for x in self.input:
            calcValues.append(self.model(x))
        
        err  = lossfunc(calcValues,self.output)
        
        self.plotError(err,path+"squard_error.png")
        maxError = np.max(err)
        minError = np.min(err)
        avgError = np.average(err)
        
        f = open(path+"report.txt", "w")
        f.write(f"Analysis Summary: \n Using loss {lossfunc.__name__}\n \n max Error: {maxError}\n min Error: {minError}\n avg Error: {avgError}")
        f.close()
        
        self.analyseParameter(path)
        
        
    def analyseParameter(self,path):
        pass
        
    def plotError(self,error, path):
        fig = pp.figure()
        axes = fig.subplots(len(self.model.inputShape))
        for ax,inp,name in zip(axes,np.transpose(self.input),self.model.inputShape):
            ax.plot(inp,error,"*")
            ax.set_title(f"error vs {name}")
        fig.legend()
        # fig.title("Sqaurd error vs Inputs")
        fig.savefig(path)
        
    def MaxAbsError(self, x_calc, x_real):
        err = np.max(np.abs(x_calc-x_real),axis=1)
        return err
            
    def MeanSquardError(self, x_calc, x_real):
        err = self.model.weight**2*(x_calc-x_real)**2
        return np.sum(err,axis=1)
    
        
    
    def graficalSettingsFrame(self,root) -> tk.Frame:
        pass    

class PerSampleFitting(FittingAlgorithm):
    def __init__(self,model,inputData, outputData) -> None:
        self.model = model
        self.set_inputData(inputData)
        self.set_outputData(outputData)
        self.history = []
        self.history_columns = model.parameter + self.model.inputShape
        self.history_index = ""

    def save_parameter(self,parameter: np.ndarray):
        self.history.append(parameter)

    def save_parameter(self,parameter: list):
        self.history = self.history + parameter

    def set_inputData(self,data):
        self.input = data
    
    def set_outputData(self,data):
        self.output = data
     
    def run(self):
        for x, y in zip(self.input,self.output):
            p,c = curve_fit(self.model.call_curveFit,x,y*self.model.weight,p0=self.model.get_parameter(),maxfev=10000)
            self.history.append(list(np.abs(p))+list(x))
            self.model.reset()
        if hasattr(self.model,"capacities"):
            for cap, values in zip(self.model.capacities,np.transpose(np.asarray(self.history))):
                cap.const = False
                cap.fitt(self.input,values)
                # if hasattr(cap,"fitt"):
                    
                #     continue
                
                # if hasattr(cap,"bounds"):
                #     bounds = cap.bounds
                # else: bounds = (-np.inf,np.inf)
                # curve_fit(cap.call_curveFit,self.input,np.log10(values),p0=cap.get(),bounds=bounds,maxfev = 10000)
            p = self.model.get_parameter()
            
        else:
            p = np.mean(np.asarray(self.history),axis=0)
            self.model.set_parameter(p)   
        return p

    def analyseParameter(self, path):
        if hasattr(self.model,"capacities"):
            reportstr = "\n\nParameter:\n"
            for name, cap in zip(self.model.parameter,self.model.capacities):
                reportstr += f" {name}: {str(cap)}\n"
            
            f = open(path+"report.txt", "a")
            f.write(reportstr)
            f.close()
            
            for name, cap, values in zip(self.model.parameter,self.model.capacities,np.transpose(np.asarray(self.history))):
                fittedValues = []
                for i in np.transpose(np.transpose(self.input)):
                    fittedValues.append(cap(*i))
                fig = pp.figure()
                axes = fig.subplots(len(self.model.inputShape))
                for ax,inp,inpname in zip(axes,np.transpose(self.input),self.model.inputShape):
                    #ax.set_title(f"{name} vs {inpname}")
                    ax.loglog(inp,values,"*")
                    ax.loglog(inp,fittedValues,"*")
                    ax.set_ylabel(f"{name} [F]")
                    ax.set_xlabel(f"{inpname} [A]")
                    ax.grid(True)
                fig.tight_layout(pad=0.5)
                fig.legend([f"Measured {name}",f"Fitted {name}"])
                fig.savefig(path + f'{name}.png')
    
    def graficalSettingsFrame(self,root) -> tk.Frame:
        pass

class GradientDescent(FittingAlgorithm):
    def __init__(self, model, inputData, outputData, learningRate = 0.001, epochs = 100,tolerance = 1e-6, inLogScale = True, parameterInLogScale = True) -> None:
        super().__init__(model, inputData, outputData)
        self.initial = None
        self.learningRate = learningRate
        self.inLogScale = inLogScale
        self.parameterInLogScale = parameterInLogScale
        self.epochs = epochs
        self.tolerance = tolerance
        self.history_columns = model.parameter + ["Avg Squard Error Loss","learningRate"]
        self.history_index = "epoch"
    
    def set_initial(self):
        self.initial = self.model.get_parameter()
       
    def run(self):
        if type(self.initial) == None:
            raise ValueError("initial Parameter not set")
       
        # Fit zeros
        self.set_initial()
        print(self.initial)
        runner = g.GradientDescent(self.model,self.input,self.output,
                                   self.initial,
                                   n_iter=self.epochs,
                                   learn_rate=self.learningRate,
                                   tolerance=self.tolerance,
                                   inLogScale=self.inLogScale,
                                   parameterInLogScale=self.parameterInLogScale)
        par, hist = runner.run()
        self.save_parameter(list(np.append(i, self.learningRate) for i in hist))
        
        # # Fit slopes
        # self.model.set_Mode("")
        # self.model.set_initialParameters(*par)
        # self.set_initial()
        # runner = g.GradientDescent(self.model,self.input,self.output,
        #                            self.initial,
        #                            n_iter=self.epochs,
        #                            learn_rate=self.learningRate,
        #                            tolerance=self.tolerance,
        #                            inLogScale=self.inLogScale,
        #                            parameterInLogScale=False)
        # par, hist = runner.run()
        # self.save_parameter(list(np.append(i, self.learningRate) for i in hist))
        
        
        
        
        return par
    
    def graficalSettingsFrame(self,root) -> tk.Frame:
        self.fittingTab = tk.Frame(root)
        
        #Algo Type
        tk.Label(self.fittingTab,text=f"Algorithm: {type(self).__name__}").grid(column=0,row=0)
        
        #Learningrate
        tk.Label(self.fittingTab,text="Learning Rate").grid(column=0,row=1)
        lrstr = tk.DoubleVar(value=self.learningRate)
        tk.Entry(self.fittingTab,textvariable=lrstr).grid(column=1,row=1)
        
        #Epochs
        tk.Label(self.fittingTab,text="Epochs").grid(column=0,row=2)
        epochstr = tk.IntVar(value=self.epochs)
        tk.Entry(self.fittingTab,textvariable=epochstr).grid(column=1,row=2)
        
        #Tolerance
        tk.Label(self.fittingTab,text="Tolerance").grid(column=0,row=3)
        tolstr = tk.DoubleVar(value=self.tolerance)
        tk.Entry(self.fittingTab,textvariable=tolstr).grid(column=1,row=3)
        
        #logscale
        tk.Label(self.fittingTab,text="Output fit in Log").grid(column=0,row=4)
        useLog = tk.BooleanVar(value=self.inLogScale)
        tk.Checkbutton(self.fittingTab,variable=useLog).grid(column=1,row=4)
        
        #parameterlogscale
        tk.Label(self.fittingTab,text="Parameter fit in Log").grid(column=0,row=5)
        parameterUseLog = tk.BooleanVar(value=self.parameterInLogScale)
        tk.Checkbutton(self.fittingTab,variable=parameterUseLog).grid(column=1,row=5)
                    
        #Savebutton
        def save():
            self.learningRate=lrstr.get()
            self.epochs=epochstr.get()
            self.tolerance=tolstr.get()
            self.inLogScale=useLog.get()
            self.parameterInLogScale=parameterUseLog.get()
        tk.Button(self.fittingTab,text="Save",command=save).grid(column=0,row=6)
        return self.fittingTab