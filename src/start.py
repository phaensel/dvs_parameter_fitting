import os
import src.dataLoader as dataLoader
from src.gui import Gui

#Fitting Algorithms
from src.fitting import *

#Noise Models
from src.NoiseModels import *

#CurrentApproximation
from src.currentApproximation import DVSipdApproximation, DVSInputApprox

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

rawDataFile = "data/noise_event_rate_array_study_with_testpixel_var_PrBp_20230107.pkl"
dataFile = "data/DVS-PSD-changing-Ipr.pkl"

report = "./report/"


initalParamerter = [3e-14,2.75e-14,1e-15]
# initalParamerter = [ 6.65760974e-14, -3.57828014e-14,  2.01843777e-15]

logScaleFitting = True
logScaleParameter = True

learningRate = 0.00000001

# 

freq = []
inputData = []
outputData = []
model = NoiseModel([],[])
fittingAlgo = FittingAlgorithm(model,inputData,outputData)

def createData(raw,file):
    dataLoader.create_data_form_raw_pickle(raw,file)

def loadData(file):
    global model, fittingAlgo
    if file == "":
        return
    freq, inputData, outputData = dataLoader.load_data(file,"pickle")
    fittingAlgo.set_inputData(inputData)
    fittingAlgo.set_outputData(outputData)
    fittingAlgo.model.set_frequencyRange(freq)

def run():
    global model, fittingAlgo
    initalParamerter = fittingAlgo.run()
    print(initalParamerter)

def export(algorithm,path = "./"):
    global model, fittingAlgo
    parameterFrame = pd.DataFrame(algorithm.history,columns=algorithm.history_columns)
    parameterFrame.index.name=algorithm.history_index
    parameterFrame.to_csv(path+"parameterHistory.csv")

def start_gui():
    global model, fittingAlgo
    gui=Gui(model,fittingAlgo)
    gui.loadFunction = loadData
    gui.fitFunction = run
    gui.exportFunction = export
    gui.show()
    
def start_without_gui(report):
    global model, fittingAlgo
    run()
    if report != "":
        os.makedirs(report)
        fittingAlgo.analyse(report)
        export(fittingAlgo,report)
    
def set_Model(modelName: str):
    global model, fittingAlgo
    if modelName == "DVSModel":
        model = DVSModel(freq)
        model.set_initialParameters(*initalParamerter)
    
    elif modelName == "DVS_linar_dependent_capacities":
        model = DVS_linar_dependent_capacities(freq)
        model.set_initialParameters(*initalParamerter)
    
    elif modelName == "DVS_physical_capacities":
        model = DVS_physical_capacities(freq)
        model.set_initialParameters(*initalParamerter)
       
    else:
        raise ValueError(f"Model {modelName} is unknown")
    print(f"Model {model.__class__.__name__} set!")
    
    
def set_Algorithm(algoName: str):
    global model, fittingAlgo
    if algoName == "PerSampleFitting":
        fittingAlgo = PerSampleFitting(model,inputData,outputData)
    
    elif algoName == "GradientDescent":
        model = GradientDescent(model,inputData,outputData,learningRate,parameterInLogScale=logScaleParameter)
        
    else:
        raise ValueError(f"Model {algoName} is unknown")
    
    print(f"Algortithm {fittingAlgo.__class__.__name__} set!")