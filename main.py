import argparse
from src.start import * 

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c","--createData",action="store_true")
    parser.add_argument("-g", "--noGui", action='store_true', help="Starting headless")

    parser.add_argument("-m", "--model", type=str, default="DVSModel", help="Name of Model")
    parser.add_argument("-a", "--algorithm", type=str, default="PerSampleFitting", help="Name of Fitting Algorithm")
    parser.add_argument("-d", "--data", type=str, default="data/DVS-PSD-changing-Ipr.pkl", help="Path to file with data to be loaded")

    parser.add_argument("--raw",nargs='+',type=str)
    parser.add_argument("-r","--report",default="", type=str,help="Path to report folder")
    parser.add_argument("--learningRate", type=float, help="Float number, only used with GradientDescent")
    # argParser.add_argument("--logScaleFitting", "--logScaleFitting", type=bool, help=", only used with GradientDescent")
    parser.add_argument('--initalParamerter', nargs='+', type=float, help='Set the inital parameter')
    parser.add_argument("--fittingParameterInLogScal", action='store_true', help="Fits parameter using log of the parameter, only used with GradientDescent")



    args = parser.parse_args()

    if (args.createData):
        if args.raw == None:
            print("Need Path to Raw file (--raw)")
            exit()
        if args.data == None:
            print("Need Path to Raw file (--data)")
            exit()
        createData(args.raw,args.data)
        exit()

#sett internal parameter
    if args.learningRate != None:
        learningRate = args.learningRate

    if args.initalParamerter != None:
        initalParamerter = args.initalParamerter
    
    logScaleParameter = args.fittingParameterInLogScal


    set_Model(args.model)
    set_Algorithm(args.algorithm)
    loadData(args.data)
    if args.noGui:
        start_without_gui(args.report)
    else:
        start_gui()
    
    







