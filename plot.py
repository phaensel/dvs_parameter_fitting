import matplotlib.pyplot as plt
import numpy
import pandas as pd

df=pd.read_csv("reports/NoBounds/parameterHistory.csv")

plt.loglog(df["Ipr"],df["Cfb"],"*")
plt.ylabel("$C_{fb}$ [F]")
plt.xlabel("$I_{pd}$ [A]")
plt.grid(True)
plt.show()

# f = numpy.loadtxt("freq.csv")

# plt.loglog(f,numpy.loadtxt("meas.csv"))
# plt.loglog(f,numpy.loadtxt("calc_cpd1321_cout1360_cfb1460.csv"))
# plt.loglog(f,numpy.loadtxt("calc_cpd1377_cout1360_cfb1519.csv"))
# plt.ylabel("$S_{vp}(f)$ $[V^2/Hz]$")
# plt.xlabel("Frequency [Hz]")
# plt.grid(True)
# plt.legend(["Measurement","Fitted PSD with Cpd=6.2E-14 and Cfb=2.5E-15","Fitted PSD with Cpd=1.7E-14 and Cfb=6.5E-16"])
# plt.show()

# from scipy.optimize import curve_fit

# class OutputCapacity:
#     def __init__(self) -> None:
#         self.zero = 1e-15
#         self.a = 5e-15
#         self.b = 1
#         self.r = 0.5
#         self.d = 0.0005
#         self.v = 1
#         self.const = False
    
#     def get(self):
#         return [self.zero, self.a, self.b, self.r,self.d,self.v]
    
#     def set(self, zero, a, b, c, d):
#         self.zero = zero
#         self.a = a
#         self.b = b
#         self.c = c
#         self.d = d
    
#     def fitt(self,x,y):
#         curve_fit(self.call_curveFit,x,y,self.get())
    
#     def call_curveFit(self, x, zero, a , b, r, d,v):
#         values = []
#         self.zero = zero
#         self.a = a
#         self.b = b
#         self.r = r
#         self.d = d
#         self.v = v
#         for cur in x:
#             values.append(self(*cur))
#         return values
    
#     def __str__(self) -> str:
#         pass
#         return f"{self.zero} + {self.a} * ({self.r}*ipr - {self.d} * numpy.log10(ipd)+{self.v}) + {self.b} * ({self.r}*ipr - {self.d} * numpy.log10(ipd)+{self.v})**2"
      
#     def __call__(self,ipd, ipr) -> float:
#         if self.const: return self.zero
#         x = self.r*ipr - self.d * numpy.log10(ipd)
#         return self.zero + self.a * (x+self.v) + self.b * (x+self.v)**2
#         #return self.zero + self.a/(1+numpy.exp(x+self.v)) + self.b/(1+numpy.exp(-x+self.v))


# df = pd.read_csv("physical/final/parameterHistory.csv")


   
# x = df[["Ipd", "Ipr"]].to_numpy() 
# cap = OutputCapacity()
# y = []

# cap.fitt(x,df["Cfb"])

# for i in x:
#     y.append(cap(*i))

# print(cap.get())

# fig = plt.figure()
# axes = fig.subplots(2)
# for ax,inp in zip(axes,numpy.transpose(x)):
#     ax.loglog(inp,y,"*")
#     ax.loglog(inp,df["Cfb"],"*")
# plt.show()